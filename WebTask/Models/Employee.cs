﻿using System;
using System.Collections.Generic;

namespace WebTask.Models
{
    public class Employee
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double Age { get; set; }
        public string Speciality { get; set; }
        public DateTime EmployementDate { get; set; }
        public string ImagePath { get; set; }
        public virtual ICollection<Task> ListOfTasks { get; set; }
    }
}
