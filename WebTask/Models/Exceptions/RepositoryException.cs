﻿using System;

namespace WebTask.Models.Exceptions
{
    public class RepositoryException : Exception
    {
        public RepositoryException(string message)
        : base(message)
        { }
    }
}
