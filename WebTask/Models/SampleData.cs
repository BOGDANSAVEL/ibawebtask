﻿using System;
using System.Linq;

namespace WebTask.Models
{
    public class SampleData
    {
        public static void Initialize(ApplicationContext context)
        {
            if (!context.Employees.Any())
            {
                context.Employees.AddRange(
                    new Employee
                    {
                        Name = "geralt of rivia",
                        Age = 88,
                        Speciality = "witcher",
                        EmployementDate = new DateTime(2011, 11, 11),
                        ImagePath = "geralt-avatar.png"
                    },
                    new Employee
                    {
                        Name = "yennefer of vengerberg",
                        Age = 190,
                        Speciality = "witch",
                        EmployementDate = new DateTime(2011, 11, 11),
                        ImagePath = "yennefer-avatar.png"
                    },
                    new Employee
                    {
                        Name = "ciri",
                        Age = 24,
                        Speciality = "witcher",
                        EmployementDate = new DateTime(2019, 11, 1),
                        ImagePath = "ciri-avatar.png"
                    },
                    new Employee
                    {
                        Name = "dandelion",
                        Age = 30,
                        Speciality = "bard",
                        EmployementDate = new DateTime(2018, 8, 11),
                        ImagePath = "dandelion-avatar.png"
                    },
                    new Employee
                    {
                        Name = "triss merigold",
                        Age = 29,
                        Speciality = "witch",
                        EmployementDate = new DateTime(2019, 10, 18),
                        ImagePath = "triss-avatar.png"
                    }
                );
                context.SaveChanges();
            }

            var geralt = context.Employees.FirstOrDefault(e => e.Name.Equals("geralt of rivia"));
            if (!context.Users.Any())
            {
                context.Users.AddRange(
                    new User
                    {
                        Login = "whitewolf",
                        Password = "evilisevil",
                        Employee = geralt,
                        EmployeeId = geralt.Id
                    }
                );
                context.SaveChanges();
            }

            if (!context.Tasks.Any())
            {
                context.Tasks.AddRange(
                    new Task
                    {
                        Name = "for geralt",
                        Description = "find ciri",
                        CompletionDate = new DateTime(2012, 12, 12),
                        Employee = geralt,
                        EmployeeId = geralt.Id
                    }
                );
                context.SaveChanges();
            }
        }
    }
}
