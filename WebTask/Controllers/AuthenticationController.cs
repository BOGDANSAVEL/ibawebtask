﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebTask.Models;
using WebTask.Models.Exceptions;
using WebTask.Services;

namespace WebTask.Controllers
{
    public class AuthenticationController : Controller
    {
        private readonly ILogger<AuthenticationController> _logger;
        private UsersService _usersService;

        public AuthenticationController(ILogger<AuthenticationController> logger, UsersService usersService)
        {
            _logger = logger;
            _usersService = usersService;

        }

        // GET: Authentication/Authenticate
        [HttpGet]
        public IActionResult Authenticate()
        {
            return View();
        }

        // POST: Authentication/Authenticate
        [HttpPost]
        public async Task<IActionResult> Authenticate([Bind("Login, Password")] AuthenticateModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    User user = await _usersService.GetAsync(model.Login, model.Password);
                    string tokenHandler = _usersService.GetJwt(user);
                    HttpContext.Session.SetString("JWToken", tokenHandler);
                } catch (RepositoryException ex)
                {
                    _logger.LogError(ex.Message, ex);
                    ViewBag.ErrorMessage = ex.Message;
                    return View();
                }
               
                return RedirectToAction("Index", "Employees");
            }
            return View();
        }


        /// <summary>
        /// Logout user. Clear the session.
        /// </summary>
        /// <returns>Redirection to Index() of EmployeesController</returns>
        public IActionResult Logout()
        {
            HttpContext.Session.Clear();
            return RedirectToAction("Index", "Employees");
        }
    }
}