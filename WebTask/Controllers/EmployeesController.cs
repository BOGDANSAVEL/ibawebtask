﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebTask.Models;
using WebTask.Models.Exceptions;
using WebTask.Services;

namespace WebTask.Controllers
{
    public class EmployeesController : Controller
    {
        private readonly ILogger<EmployeesController> _logger;
        private readonly ApplicationContext _context;
        private readonly EmployeesService _employeesService;
        private readonly UsersService _usersService;

        public EmployeesController(ILogger<EmployeesController> logger, ApplicationContext context, EmployeesService employeesService, UsersService usersService)
        {
            _logger = logger;
            _context = context;
            _employeesService = employeesService;
            _usersService = usersService;
        }

        // GET: Employees
        public async Task<IActionResult> Index()
        {
            IEnumerable<Employee> employees = await _employeesService.GetAllAsync();
            ViewBag.Specialitys = employees.Select(e => e.Speciality).Distinct();
            return View(employees);
        }

        // GET: Employees/Search?name=&age=&date=&commit=Search
        public async Task<IActionResult> Search(string name, string speciality, int? age, DateTime employementDate)
        {
            ViewBag.Specialitys = (await _employeesService.GetAllAsync()).Select(e => e.Speciality).Distinct();
            return View("Index", await _employeesService.SearchAsync(name, speciality, age, employementDate));
        }

        // GET: Employees/PersonalPage
        public async Task<IActionResult> PersonalPage()
        {
            var employee = await _usersService.GetEmployeeAsync(HttpContext.User.Identity.Name);
            return RedirectToAction("Details", new { id = employee.Id});
        }

        // GET: Employees/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            try
            {
                return View(await _employeesService.GetByIdIncludeTasksAsync(id));
            } catch (RepositoryException ex)
            {
                _logger.LogError(ex.Message, ex);
                return NotFound();
            }
        }

        // GET: Employees/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Employees/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Age,Speciality,EmployementDate,ImagePath")] Employee employee)
        {
            if (ModelState.IsValid)
            {
                await _employeesService.AddAsync(employee);
                return RedirectToAction(nameof(Index));
            }
            return View(employee);
        }

        // GET: Employees/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            try
            {
                return View(await _employeesService.GetByIdAsync(id));
            } catch (RepositoryException)
            {
                return NotFound();
            }
        }

        // POST: Employees/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Age,Speciality,EmployementDate,ImagePath")] Employee employee)
        {
            if (id != employee.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    await _employeesService.UpdateAsync(employee);
                } catch (RepositoryException ex)
                {
                    _logger.LogError(ex.Message, ex);
                    return NotFound();
                }
                return RedirectToAction(nameof(Index));
            }
            return View(employee);
        }

        // GET: Employees/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            try
            {
                return View(await _employeesService.GetByIdAsync(id));
            } catch (RepositoryException ex)
            {
                _logger.LogError(ex.Message, ex);
                return NotFound();
            }
        }

        // POST: Employees/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int? id)
        {
            try
            {
                await _employeesService.DeleteById(id);
                return RedirectToAction(nameof(Index));
            } catch (RepositoryException ex)
            {
                _logger.LogError(ex.Message, ex);
                return NotFound();
            }
        }
    }
}
