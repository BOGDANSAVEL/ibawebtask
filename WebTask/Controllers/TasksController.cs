﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebTask.Models;
using WebTask.Models.Exceptions;
using WebTask.Services;

namespace WebTask.Controllers
{
    public class TasksController : Controller
    {
        private readonly ILogger<TasksController> _logger;
        private TasksService _taskService;
        private EmployeesService _employeesService;

        public TasksController(ILogger<TasksController> logger, TasksService tasksService, EmployeesService employeesService)
        {
            _logger = logger; 
            _taskService = tasksService;
            _employeesService = employeesService;

        }

        // GET: Tasks/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            try
            {
                return View(await _taskService.GetByIdAsync(id));
            } catch (RepositoryException ex)
            {
                _logger.LogError(ex.Message, ex);
                return NotFound();
            }
        }

        // GET: Tasks/Create
        public async Task<IActionResult> Create(int? id)
        {
            try
            {
                ViewBag.EmployeeId = (await _employeesService.GetByIdAsync(id)).Id;
                return View();
            } catch (RepositoryException ex)
            {
                _logger.LogError(ex.Message, ex);
                return NotFound();
            }
        }

        // POST: Tasks/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Name,Description,CompletionDate")] Models.Task task, int? employeeId)
        {
            ViewBag.EmployeeId = employeeId;
            if (ModelState.IsValid)
            {
                try
                {
                    await _taskService.CreateAsync(task, employeeId);
                    return RedirectToAction("Details", "Employees", new { id = employeeId });
                } catch (RepositoryException ex)
                {
                    _logger.LogError(ex.Message, ex);
                    return NotFound();
                }
            }
            return View(task);
        }

        // GET: Tasks/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            try
            {
                return View(await _taskService.GetByIdAsync(id));
            } catch (RepositoryException ex)
            {
                _logger.LogError(ex.Message, ex);
                return NotFound();
            }
        }

        // POST: Tasks/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Description,CompletionDate,EmployeeId")] Models.Task task)
        {
            if (id != task.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    await _taskService.UpdateAsync(task);
                    return RedirectToAction("Details", "Employees", new { id = task.EmployeeId });
                } catch (RepositoryException ex)
                {
                    _logger.LogError(ex.Message, ex);
                    return NotFound();
                }
            }
            return View(task);
        }

        // GET: Tasks/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            try
            {
                return View(await _taskService.GetByIdAsync(id));
            }
            catch (RepositoryException ex)
            {
                _logger.LogError(ex.Message, ex);
                return NotFound();
            }
        }

        // POST: Tasks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int? id)
        {
            try
            {
                Employee employee = await _taskService.GetEmployee(id);
                await _taskService.DeleteById(id);
                return RedirectToAction("Details", "Employees", new { id = employee.Id });
            } catch (RepositoryException ex)
            {
                _logger.LogError(ex.Message, ex);
                return NotFound();
            }
        }        
    }
}
