﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Threading.Tasks;
using WebTask.Models;
using WebTask.Models.Exceptions;

namespace WebTask.Services
{
    public class TasksService
    {
        private const string WrongIdMessage = "There is no task with such id";

        private readonly ILogger<TasksService> _logger;
        private readonly ApplicationContext _context;

        public TasksService(ILogger<TasksService> logger, ApplicationContext context)
        {
            _logger = logger;
            _context = context;
        }

        public async Task<Models.Task> GetByIdAsync(int? id)
        {
            if (id == null)
            {
                throw new RepositoryException(WrongIdMessage);
            }

            var task = await _context.Tasks
                .FirstOrDefaultAsync(m => m.Id == id);
            if (task == null)
            {
                throw new RepositoryException(WrongIdMessage);
            }

            return task;
        }

        public async Task<Employee> GetEmployee(int? id)
        {
            if (id == null)
            {
                throw new RepositoryException(WrongIdMessage);
            }

            var task = await _context.Tasks
                .Include(t => t.Employee)
                .FirstOrDefaultAsync(t => t.Id == id);
            if (task == null)
            {
                throw new RepositoryException(WrongIdMessage);
            }

            return task.Employee;
        }

        public async System.Threading.Tasks.Task CreateAsync(Models.Task task, int? employeeId)
        {
            if (employeeId == null)
            {
                throw new RepositoryException(WrongIdMessage);
            }

            Employee employee = await _context.Employees
                .FirstOrDefaultAsync(m => m.Id == employeeId);
            if (employee == null)
            {
                throw new RepositoryException(WrongIdMessage);
            }
            task.Employee = employee;
            _context.Add(task);
            await _context.SaveChangesAsync();
        }

        public async System.Threading.Tasks.Task UpdateAsync(Models.Task task)
        {
            try
            {
                _context.Update(task);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                _logger.LogError(ex.Message, ex);

                if (!TaskExists(task.Id))
                {
                    throw new RepositoryException(WrongIdMessage);
                }
                else
                {
                    throw;
                }
            }
        }

        public async System.Threading.Tasks.Task DeleteById(int? id)
        {
            Models.Task task = await GetByIdAsync(id);
            _context.Tasks.Remove(task);
            await _context.SaveChangesAsync();
        }

        private bool TaskExists(int? id)
        {
            return _context.Tasks.Any(e => e.Id == id);
        }
    }
}
