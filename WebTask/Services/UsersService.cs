﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using WebTask.JWT;
using WebTask.Models;
using WebTask.Models.Exceptions;

namespace WebTask.Services
{
    public class UsersService
    {
        private const string WrongCredentialsMessage = "There is no user with such login/password";

        private readonly ILogger<UsersService> _logger;
        private readonly ApplicationContext _context;

        public UsersService(ILogger<UsersService> logger, ApplicationContext context)
        {
            _logger = logger;
            _context = context;
        }

        /// <summary>
        /// Generate jwt token for the user.
        /// </summary>
        /// <param name="user"></param>
        /// <returns>jwt handler</returns>
        public string GetJwt(User user)
        {
            var identity = GetIdentity(user);

            var now = DateTime.UtcNow;

            var jwt = new JwtSecurityToken(
                    issuer: AuthOptions.Issure,
                    audience: AuthOptions.Audience,
                    notBefore: now,
                    claims: identity.Claims,
                    expires: now.Add(TimeSpan.FromMinutes(AuthOptions.Lifetime)),
                    signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);
            return encodedJwt;
        }

        public async Task<User> GetAsync(string login, string password)
        {
            var user = await _context.Users
                .FirstOrDefaultAsync(x => x.Login == login && x.Password == password);
            if (user == null)
            {
                throw new RepositoryException(WrongCredentialsMessage);
            }
            return user;
        }

        public async Task<Employee> GetEmployeeAsync(string login)
        {
            var employee = await _context.Users.Where(u => u.Login == login).Select(u => u.Employee).FirstOrDefaultAsync();
            if (employee == null)
            {
                throw new RepositoryException(WrongCredentialsMessage);
            }
            return employee;
        }

        /// <summary>
        /// Generate Identity Claims for the user.
        /// </summary>
        /// <param name="user">user value</param>
        /// <returns></returns>
        private ClaimsIdentity GetIdentity(User user)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, user.Login)
            };
            ClaimsIdentity claimsIdentity =
            new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType,
                ClaimsIdentity.DefaultRoleClaimType);
            return claimsIdentity;
        }
    }
}
