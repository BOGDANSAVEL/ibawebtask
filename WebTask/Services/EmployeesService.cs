﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebTask.Models;
using Microsoft.EntityFrameworkCore;
using WebTask.Models.Exceptions;
using Microsoft.Extensions.Logging;

namespace WebTask.Services
{
    public class EmployeesService
    {
        private const string WrongIdMessage = "There is no employee with such id";

        private readonly ILogger<EmployeesService> _logger;
        private readonly ApplicationContext _context;

        public EmployeesService(ILogger<EmployeesService> logger, ApplicationContext context)
        {
            _logger = logger;
            _context = context;
        }

        public async Task<IEnumerable<Employee>> GetAllAsync()
        {
            return await _context.Employees.ToListAsync();
        }

        /// <summary>
        /// Search for specific Employees by name, speciality, age and employementDate
        /// </summary>
        /// <param name="name">name value</param>
        /// <param name="speciality">speciality value</param>
        /// <param name="age">age value</param>
        /// <param name="employementDate">employementDate value</param>
        /// <returns>IEnumerable of specific Employees</returns>
        public async Task<IEnumerable<Employee>> SearchAsync(string name, string speciality, int? age, DateTime employementDate)
        {
            return await _context.Employees
                            .Where(e =>
                            (name == null || e.Name.Equals(name)) &&
                            (speciality == null || e.Speciality.Equals(speciality)) &&
                            (age == null || e.Age == age) &&
                            (employementDate == DateTime.MinValue || e.EmployementDate.Equals(employementDate)))
                            .ToListAsync();
        }

        /// <summary>
        /// Gets Employee entity including Tasks navigation property.
        /// </summary>
        /// <param name="id">id value</param>
        /// <returns></returns>
        public async Task<Employee> GetByIdIncludeTasksAsync(int? id)
        {
            if (id == null)
            {
                throw new RepositoryException(WrongIdMessage);
            }

            Employee employee = await _context.Employees
                .Include(e => e.ListOfTasks)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (employee == null)
            {
                throw new RepositoryException(WrongIdMessage);
            }
            return employee;
        }

        public async Task<Employee> GetByIdAsync(int? id)
        {
            if (id == null)
            {
                throw new RepositoryException(WrongIdMessage);
            }

            var employee = await _context.Employees.FindAsync(id);
            if (employee == null)
            {
                throw new RepositoryException(WrongIdMessage);
            }
            return employee;
        }

        public async System.Threading.Tasks.Task AddAsync(Employee employee)
        {
            _context.Add(employee);
            await _context.SaveChangesAsync();
        }

        public async System.Threading.Tasks.Task UpdateAsync(Employee employee)
        {
            try
            {
                _context.Update(employee);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                _logger.LogError(ex.Message, ex);

                if (!EmployeeExists(employee.Id))
                {
                    throw new RepositoryException(WrongIdMessage);
                }
                else
                {
                    throw;
                }
            }
        }

        public async System.Threading.Tasks.Task DeleteById(int? id)
        {
            Employee employee = await GetByIdAsync(id);
            _context.Employees.Remove(employee);
            await _context.SaveChangesAsync();
        }

        private bool EmployeeExists(int id)
        {
            return _context.Employees.Any(e => e.Id == id);
        }

    }
}
