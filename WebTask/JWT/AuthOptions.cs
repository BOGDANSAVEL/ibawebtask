﻿using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace WebTask.JWT
{
    public class AuthOptions
    {
        public const string Issure = "MyAuthServer";
        public const string Audience = "http://localhost:51884/";
        public const int Lifetime = 1;

        private const string Key = "mysupersecret_secretkey!";

        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Key));
        }
    }
}
